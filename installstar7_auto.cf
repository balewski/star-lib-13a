bundle agent installstar_auto
{
 vars:
	#Login files for root (.cshrc & .login).
        "root_cshrc" 		string => "/root/.cshrc";
        "root_login" 		string => "/root/.login";

	#Login files for staruser (.cshrc & .login).
	"staruser_cshrc" 	string => "/star/u/staruser/.cshrc";
	"staruser_login" 	string => "/star/u/staruser/.login";
	
	#User data for the staruser account (shell, UID, GID, etc).
	"staruser_data" 	string => "staruser -d /star/u/staruser -s /bin/tcsh -m -u 1669 -g 31012";

	#Directories for /star/u/staruser & Star Software Install directories.
	"star_d" 		string => "/star";
	"star_u_d" 		string => "/star/u";
	"staruser" 		string => "/star/u/staruser";
	"cern_d" 		string => "/cern";
	"opt_star_d" 		string => "/opt/star";
	"usr_local_star_d" 	string => "/usr/local/star";

 classes:
	#The following classes check if the needed directories & files exists for the STAR Software Install.
        "software_directory"    	expression => fileexists("/root/Software");
        "have_script"           	expression => fileexists("/root/Software/installstar");
        "have_flexlibs"         	expression => fileexists("/root/Software/flex32libs-2.5.35-8.el6.i386.rpm");
        "have_mysql_static"     	expression => fileexists("/root/Software/mysql-devel-static-5.1.67-1.el6.x86_64.rpm");
        "cern"                  	expression => fileexists("/cern");
        "opt_star"              	expression => fileexists("/opt/star");
        "usr_local_star"        	expression => fileexists("/usr/local/star");
	"usr_local_star_packages_conf"	expression => fileexists("/usr/local/star/packages/conf");
        "star"                  	expression => fileexists("/star");
        "star_u"                	expression => fileexists("/star/u");
        "star_u_staruser"        	expression => fileexists("/star/u/staruser");
	
	#The following classes check if the files exists or not for the needed packages. 
	#If the files dont exist then it marks the class name as unsolved and attempts to install the package
        "tcsh"          		expression => fileexists("/bin/tcsh");
        "libXpm"        		expression => fileexists("/usr/lib/libXpm.so.4");
        "libXext"       		expression => fileexists("/usr/lib/libXext.so.6");
        "libXrender"    		expression => fileexists("/usr/lib/libXrender.so.1");
        "libstdc"       		expression => fileexists("/usr/lib/libstdc++.so.6");
        "fontconfig"    		expression => fileexists("/usr/lib/libfontconfig.so.1");
        "zlib"          		expression => fileexists("/lib/libz.so.1");
        "libgfortran"   		expression => fileexists("/usr/lib/libgfortran.so.3");
        "libSM"         		expression => fileexists("/usr/lib/libSM.so.6");
        "mysql_libs"    		expression => fileexists("/usr/lib/mysql/libmysqlclient.so.16");
        "gcc_c"         		expression => fileexists("/usr/bin/g++"); #(SL 6)
        "gcc44_c"       		expression => fileexists("/usr/bin/g++44"); #(SL 5)
        "gcc_gfortran"  		expression => fileexists("/usr/bin/gfortran");  #(SL 6)
        "gcc44_gfortran" 		expression => fileexists("/usr/bin/gfortran44"); #(SL 5)
        "glibc_devel"   		expression => fileexists("/usr/lib/gcrt1.o");
        "xorg_x11_xauth" 		expression => fileexists("/usr/bin/xauth");
        "wget"          		expression => fileexists("/usr/bin/wget");
        "make"          		expression => fileexists("/usr/bin/make");
        "libxml2"       		expression => fileexists("/usr/lib/libxml2.so.2");
        "flexlibs"      		expression => fileexists("/usr/lib/libfl.a");
        "mysql_static"  		expression => fileexists("/usr/lib/mysql/libmysqlclient.a");

	#These two classes check if the rhstar group exists and if the staruser exists.
        "group_rhstar"    		expression => groupexists("rhstar");
        "user_staruser"   		expression => userexists("staruser");

 files:
	"$(root_cshrc)"
        create => "true",
        edit_line => cshrc_edit,
        classes => if_repaired("root_cshrc_repaired");

        "$(root_login)"
        create => "true",
#        perms => login_perms,
        edit_line => login_edit,
        classes => if_repaired("root_login_repaired");

   have_script::
	"/root/Software/installstar"
        perms => star_perms;


   star::
	"$(star_d)"
	perms => home_perms;
	
   star_u::
	"$(star_u_d)"
	perms => home_perms;

   star_u_staruser.user_staruser::
	"$(staruser)"
	perms => staruser_own,
	depth_search => u_recurse("inf"),
	classes => if_repaired("staruser_own_repaired");
   star_u_staruser.user_staruser::
        "$(staruser)"
        perms => staruser_own,
        classes => if_repaired("staruser_own_repaired");

   cern.user_staruser::
	"$(cern_d)"
        perms => staruser_own,
        depth_search => u_recurse("inf"),
        classes => if_repaired("staruser_own_repaired");
   cern.user_staruser::
        "$(cern_d)"
        perms => staruser_own,
        classes => if_repaired("staruser_own_repaired");

   opt_star.user_staruser::
	"$(opt_star_d)"
        perms => staruser_own,
        depth_search => u_recurse("inf"),
        classes => if_repaired("staruser_own_repaired");
   opt_star.user_staruser::
        "$(opt_star_d)"
        perms => staruser_own,
        classes => if_repaired("staruser_own_repaired");

   usr_local_star.user_staruser::
	"$(usr_local_star_d)"
        perms => staruser_own,
        depth_search => u_recurse("inf"),
        classes => if_repaired("staruser_own_repaired");
   usr_local_star.user_staruser::
        "$(usr_local_star_d)"
        perms => staruser_own,
        classes => if_repaired("staruser_own_repaired");

   user_staruser::
	"$(staruser_cshrc)"
        create => "true",
        edit_line => cshrc_edit,
        classes => if_repaired("staruser_cshrc_repaired");

        "$(staruser_login)"
        create => "true",
 #       perms => login_perms,
        edit_line => login_edit,
        classes => if_repaired("staruser_login_repaired");

 commands:
   #This bunch of yum installs is all of the required packages for Star Software to run
#jan-out   scientific_6.!user_staruser::
#jan-out    "/usr/bin/yum -y update";
   scientific_6.!tcsh::
        "/usr/bin/yum -y install tcsh";
   scientific_6.!libXpm::
        "/usr/bin/yum -y install libXpm.i686";
   scientific_6.!libXext::
        "/usr/bin/yum -y install libXext.i686";
   scientific_6.!libXrender::
        "/usr/bin/yum -y install libXrender.i686";
   scientific_6.!libstdc::
        "/usr/bin/yum -y install libstdc++.i686";
   scientific_6.!fontconfig::
        "/usr/bin/yum -y install fontconfig.i686";
   scientific_6.!zlib::
        "/usr/bin/yum -y install zlib.i686";
   scientific_6.!libgfortran::
        "/usr/bin/yum -y install libgfortran.i686";
   scientific_6.!libSM::
        "/usr/bin/yum -y install libSM.i686";
   scientific_6.!mysql_libs::
        "/usr/bin/yum -y install mysql-libs.i686";
   scientific_6.!gcc_c::
        "/usr/bin/yum -y install gcc-c++";
   scientific_6.!gcc_gfortran::
       "/usr/bin/yum -y install gcc-gfortran";
   scientific_6.!glibc_devel::
        "/usr/bin/yum -y install glibc-devel.i686";
   scientific_6.!xorg_x11_xauth::
        "/usr/bin/yum -y install xorg-x11-xauth";
   scientific_6.!wget::
        "/usr/bin/yum -y install wget";
   scientific_6.!make::
        "/usr/bin/yum -y install make";
   scientific_6.!libxml2::
       "/usr/bin/yum -y install libxml2.i686";

   #The following commands will check if the addidtional package FLEXLIBS ininstalled, if not it uses wget to grab file then install.
   scientific_6.!have_flexlibs::
	"/usr/bin/wget -N -P /root/Software http://www.star.bnl.gov/irmo/installstar/flex32libs-2.5.35-8.el6.i386.rpm";
   scientific_6.have_flexlibs.!flexlibs::
	"/bin/rpm -i /root/Software/flex32libs-2.5.35-8.el6.i386.rpm";

   #The following commands will check if the addidtional package MYSQL-DEVEL-STATIC  ininstalled, if not it uses wget to grab file then install.
   scientific_6.!have_mysql_static::
	"/usr/bin/wget -N -P /root/Software http://www.star.bnl.gov/irmo/installstar/mysql-devel-static-5.1.67-1.el6.x86_64.rpm";
   scientific_6.have_mysql_static.!mysql_static::
	"/bin/rpm -i /root/Software/mysql-devel-static-5.1.67-1.el6.x86_64.rpm";

  #The following commands make sure the /rootSoftware directory exists and the installstar script has been downloaded
  !software_directory::
        "/bin/mkdir /root/Software";
  !have_script::
        "/usr/bin/wget -N -P /root/Software http://www.star.bnl.gov/irmo/installstar/installstar";
  
  #The following command will run the installstar script but only until all of the yum packages and the two addidtion packages have been installed.
  scientific_6.tcsh.libXpm.libXext.libXrender.libstdc.fontconfig.zlib.libgfortran.libSM.gcc_c.glibc_devel.xorg_x11_xauth.wget.make.mysql_libs.libxml2.flexlibs.mysql_static.!cern.!opt_star.!usr_local_star::
        "/root/Software/installstar SL13a";

   #This command will create the /star/u diectory if it does not currently exists.
   !star_u::
        "/bin/mkdir -p /star/u";

   #This command will add the group "rhstar" with GID 31012 if it does not currently exists
   !group_rhstar::
	"/usr/sbin/groupadd -g 31012 rhstar";

   #This command will create the user "staruser" but not until the Star Software has been fully installed. Also once this user is created it will prevent "yum update" from running every time CFEngine makes a pass.
   cern.opt_star.usr_local_star.usr_local_star_packages_conf.group_rhstar.!user_staruser::
	"/usr/sbin/useradd $(staruser_data)";



 reports:
   root_cshrc_repaired::
        "root cshrc has been repaired";

   root_login_repaired::
        "root login has been repaired";

   staruser_cshrc_repaired::
        "staruser cshrc has been repaired";

   staruser_login_repaired::
        "staruser login has been repaired";


  !tcsh.!libXpm.!libXext.!libXrender.!libstdc.!fontconfig.!zlib.!libgfortran.!libSM.!gcc_c.!glibc_devel.!xorg_x11_xauth.!wget.!make.!mysql_libs.!libxml2.!flexlibs.!mysql_static::
        "All of the required packages have NOT been installed! :-(";

   tcsh.libXpm.libXext.libXrender.libstdc.fontconfig.zlib.libgfortran.libSM.gcc_c.glibc_devel.xorg_x11_xauth.wget.make.mysql_libs.libxml2.flexlibs.mysql_static::
        "All of the required packges have been installed! :-)";

   !cern.!opt_star.!usr_local_star::
        "The installstar script has NOT been installed! :-(";

   cern.opt_star.usr_local_star::
        "The installstar script has been installed! :-)";

   !user_staruser::
	"The user staruser does not exist and I will attempt to create it. (only if Star Software is installed)";

   user_staruser::
	"The user staruser has been created and already exists! :-)";

   !group_rhstar::
	"The rhstar group does not exists and I will attemp to create it.";

   group_rhstar::
	"The rhstar group has been created and already exists! :-)";

   staruser_own_repaired::
	"The ownership of the directories /star/u/staruser, /cern, /opt/star & /usr/local/star have been changed recursively to staruser";  

}

body perms star_perms
{
        mode => "700";
}

bundle edit_line cshrc_edit
{
        insert_lines:
        any::
        "source /usr/local/star/group/templates/cshrc";
}

bundle edit_line login_edit
{
        insert_lines:
        any::
        "source /usr/local/star/group/templates/login";
}

body perms login_perms
{
        mode => "644";
}

body perms home_perms
{
	mode => "755";
}

body perms staruser_own
{
        owners  => {"staruser"};
	groups  => {"rhstar"};
}

body depth_search u_recurse(d)
{
	depth => "$(d)";
}

