#!/usr/bin/env bash
echo "-------- copy STAR CFE polices in to /var/cfengine area ---"
cp installstar_exec.cf /var/cfengine/masterfiles/
cp promises.cf-star7  /var/cfengine/masterfiles/promises.cf

echo "-------- master specific -------"
/sbin/ifconfig eth0 |grep Mask
MYIP=`/sbin/ifconfig eth0 |grep Mask |cut -f2 -d: |cut -f1 -d\ `
echo MYIP=$MYIP
echo "    /var/cfengine/bin/cf-agent --bootstrap --policy-server 10.10.0.17xxx"

echo "exec       cf-agent -KI -f ./ex_config_iptables.cf  "

echo "exec       cf-agent -KI -f /var/cfengine/masterfiles/installstar_exec.cf "

