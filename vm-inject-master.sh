#!/bin/bash
echo "********JAN: injecting STAR configuration w/ CFE  *********"
yum install telnet -y 
pwd
wget https://bitbucket.org/balewski/star-lib-13a/get/master.zip
if [ $? -ne 0  ]; then echo "cfe policy  not found in bitbucket, aborting STAR-lib installation" ; exit 1; fi

unzip master.zip 
CFE_DIR=`ls  |grep balewski-star-lib-13a `
echo 'CFE policy is in dir=$CFE_DIR\='
cd $CFE_DIR
if [ $? -ne 0  ]; then
    echo "failed unpacking CFE policy, aborting STAR-lib installation ";
    pwd
    ls -l ./
    exit 3
fi

MYIP=`/sbin/ifconfig eth0 |grep Mask |cut -f2 -d: |cut -f1 -d\ `
if [ $? -ne 0  ]; then
    echo "local IP not found, aborting STAR-lib installation ";
    /sbin/ifconfig
    exit 2
fi
echo " my local IP=$MYIP "
NEWNAME=master-`echo $MYIP | cut -f4 -d.`
hostname $NEWNAME
echo -n "new hostname is "
hostname

/var/cfengine/bin/cf-agent --bootstrap --policy-server $MYIP
if [ $? -ne 0  ]; then
    echo "self-boostrap of CFE failed, aborting STAR-lib installation ";
    /var/cfengine/bin/cf-agent
    exit 2
fi
/var/cfengine/bin/cf-agent -KI -f ./ex_config_iptables.cf
cp -f installstar7_auto.cf /var/cfengine/masterfiles/
cp -f promises.cf-star7  /var/cfengine/masterfiles/promises.cf
echo "********JAN: end - CFE got all the input, now wait for 20 minutes ******"
iptables-save 
/var/cfengine/bin/cf-agent
